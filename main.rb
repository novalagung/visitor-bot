require 'rubygems'
require 'mechanize'

@proxies    = []
@user_agent = []
blog 		= 'http://farhanfatur.blogspot.com/2013/10/bersiap-siap-untuk-game-assassins-creed.html'

File.open('proxy.txt').read.each_line do |proxy|
	@proxies.push(proxy.split(/:/))
end

File.open('user-agent.txt').read.each_line do |user_agent|
	@user_agent.push(user_agent)
end

while true
	@proxies.shuffle.each do |proxy|
		@user_agent.shuffle.each do |user_agent|
			puts "Trying to access #{blog} from #{proxy[0]} using user agent #{user_agent}"
			begin
				mechanize = Mechanize.new
				mechanize.set_proxy(proxy[0].to_s, proxy[1].to_s)
				mechanize.user_agent 	= user_agent
				mechanize.open_timeout	= 10
				mechanize.read_timeout	= 10
				mechanize.get(blog, :referer => 'http://you.com/') do |page|
					i = 0
					page.links.shuffle.each do |link|
						next  if Random.rand(10) == 5
						next  if link.href.to_s.strip == ""
						break if Random.rand(10) == 5 and ++i > 0
						link.click
						puts "------- clicking #{link.text.strip[0, 10]}"
						sleep(Random.rand(10))
					end
					puts "--- done"
				end
			rescue
				puts "--- failed"
				break
			end
		end
	end
end